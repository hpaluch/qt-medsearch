#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_about.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_czLocale("cs")
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initDb(const QString &dbName)
{
    QFileInfo info(dbName);
    if (!info.exists()){
        fatalError("Database file '"+info.absoluteFilePath()+"' does not exists or is not accesible");
    }
    if (!info.isReadable()){
        fatalError("Database file '"+info.absoluteFilePath()+"' is not readable");
    }
    if (info.size() == 0L){
        fatalError("Database file '"+info.absoluteFilePath()+"' is empty (0 bytes length)");
    }

    // initialization taken from https://katecpp.wordpress.com/2015/08/28/sqlite-with-qt/
    m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(info.absoluteFilePath());
    if(!m_db.open()){
        fatalError(m_db.lastError().text()+"for database "+dbName);
    }
    if (m_db.tables().count()==0){
        fatalError("No table found in database '"+dbName+"'\r\nPossible cause: database not found or not initialized");
    }

    // model setup taken from http://doc.qt.io/qt-5/qsqlquerymodel.html#details
    m_model = new QSqlQueryModel;
    m_model->setQuery("SELECT sukl_code,name FROM sukl_medicaments");
    m_model->setHeaderData(0, Qt::Horizontal, tr("SUKL Code"));
    m_model->setHeaderData(1, Qt::Horizontal, tr("Name"));
    ui->tableView->setModel(m_model);
    ui->statusBar->showMessage("Loaded database '"+info.absoluteFilePath()+"'");
    m_dbPath = info.absoluteFilePath();
}

void MainWindow::fatalError(const QString& details)
{
    // taken from http://www.qtcentre.org/archive/index.php/t-20759.html
    QMessageBox mbox(
    QMessageBox::Critical,
        tr("Fatal Error"),
        tr("%1 has encountered an error and cannot continue to work.\n"
        "Please press OK button to quit.").arg(qApp->applicationName()),
        QMessageBox::Ok,
    this);
    mbox.setDetailedText(details);
    mbox.exec();

    // FIXME: proper detection of GUI state!!!
    if (isVisible()){
        qApp->exit();
    } else {
        exit(EXIT_FAILURE);
    }

}

void MainWindow::on_lineEdit_editingFinished()
{
    QString name = m_czLocale.toUpper(ui->lineEdit->text());

    // avoid unnecessary reloads/status cleanups....
    if ( name == m_oldName){
        return;
    }
    m_oldName = name;

    //qDebug("Name is '"+name+"'");

    QSqlQuery query;
    query.prepare("SELECT sukl_code,name FROM sukl_medicaments where name like :name or sukl_code like :code");
    // we are searching code only as starting string
    query.bindValue(":code",name+"%");
    // name searching as "fulltext"
    query.bindValue(":name", "%"+name+"%");
    query.exec();
    m_model->setQuery(query);
    if (name.length()>0){
        ui->statusBar->showMessage("Filtering by '"+name+"'");
    } else {
        ui->statusBar->clearMessage();
    }
}

void MainWindow::on_actionE_xit_triggered()
{
    qApp->quit();
}

void MainWindow::on_action_Find_triggered()
{
    on_lineEdit_editingFinished();
}

void MainWindow::on_action_Reset_Find_triggered()
{
    ui->lineEdit->setText("");
    on_lineEdit_editingFinished();
}

void MainWindow::on_actionAbout_triggered()
{
    QDialog *about = new QDialog(0,0);

    Ui_Dialog aboutUi;
    aboutUi.setupUi(about);

    qDebug() << "appName: " << qApp->applicationName();
    qDebug() << "appVersion: " << qApp->applicationVersion();
    qDebug() << "appDisplayName: " << qApp->applicationDisplayName();
    qDebug() << "appPath" << qApp->applicationFilePath();
    qDebug() << "appPid" << qApp->applicationPid();

    aboutUi.labelAppNameValue->setText(qApp->applicationName()+" ("+QString::number(qApp->applicationPid())+")");
    aboutUi.labelAppPathValue->setText(qApp->applicationFilePath());
    aboutUi.labelDbFileValue->setText(m_dbPath);
    aboutUi.labelWinNameValue->setText(this->windowTitle());
    aboutUi.labelQtBuildVerValue->setText(QT_VERSION_STR);
    aboutUi.labelQtRuntimeVerValue->setText(qVersion());
    about->exec();
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    qApp->aboutQt();
}
