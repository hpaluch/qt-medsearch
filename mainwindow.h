#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDebug>

#include <QFileInfo>

#include <QLocale>

#include <QMainWindow>
#include <QMessageBox>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlQueryModel>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void initDb(const QString &dbName);
    void fatalError(const QString& details);

private slots:
    void on_lineEdit_editingFinished();

    void on_actionE_xit_triggered();

    void on_action_Find_triggered();

    void on_action_Reset_Find_triggered();

    void on_actionAbout_triggered();

    void on_actionAbout_Qt_triggered();

private:
    Ui::MainWindow *ui;
    QSqlDatabase m_db;
    QSqlQueryModel *m_model;
    QLocale m_czLocale;
    QString m_oldName;
    QString m_dbPath; // information in about box
};

#endif // MAINWINDOW_H
