Sample Qt application to search Medicaments
===========================================

This simple Qt 5 application is supposed to show basic
usage of Table View, SQLite and related technologies.

Implemented features
--------------------
* browsing data from SQLite
* search data by name (fulltext) and SUKL Code (beginning text)


Setup for Ubuntu 14.04.4 LTS
============================

In all cases install qtcreator (it is usefull for dependencies):

	sudo apt-get install qtcreator sqlite libqt5sql5-sqlite

In case of SSE2 CPU (all 64-bit CPUs should be fine) then run:

	qtcreator


Workaround for old 32bit, non SSE2 CPUs
=======================================

* get old version of Qt Creator from: http://ftp.fau.de/qtproject/official_releases/qtcreator/2.7/2.7.2/qt-creator-linux-x86-opensource-2.7.2.bin
* change permissions and run that installer:

		chmod a+rx qt-creator-linux-x86-opensource-2.7.2.bin 
		./qt-creator-linux-x86-opensource-2.7.2.bin 

* from X-Window run (adjust path if you changed it):

		~/qtcreator-2.7.2/bin/qtcreator

Common instructions
-------------------

After build in Qt Creator do not forget to copy database from project
to build directory, for example:

	cd qt-medsearch
	cp medsearch.db ../build-qt-medsearch-Desktop-Debug/

Rebuild of medicaments database
===============================

To create empty database "medsearch.db" run:

	setup/10_create_db.sh

Get database ZIP file from https://opendata.sukl.cz/?q=katalog/databaze-lecivych-pripravku-dlp

Extract dlp_lecivepripravky.csv from above ZIP file

Prepare sample data (these are included in this source as sample.csv)

	cd setup
	head -100 path_to_extracted_zip/DLP/dlp_lecivepripravky.csv |
	 ./20_convert_data.sh  > sample.sql

Note: if want full database issue instead:

	 ./20_convert_data.sh <  path_to_extracted_zip/DLP/dlp_lecivepripravky.csv > full.sql


Load sample data into database (be in "setup" subdirectory):

	./30_load_data.sh sample.sql


Resources
=========
There were used meny resources to make this app including:

* on form layout: http://doc.qt.io/qt-5/layout.html
* on sqlquery model: http://doc.qt.io/qt-5/qsqlquerymodel.html#details
* standard icon names (type in Theme of Action)
  https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
* getting Qt versions: http://linux.m2osw.com/find-qt-version-command-line-compile-time-run-time
* fast transactions in SQLite: http://blog.quibb.org/2010/08/fast-bulk-inserts-into-sqlite/


