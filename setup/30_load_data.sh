#!/bin/bash

[ $# -eq 1 ] || {
	echo "Usage: $0 sql_file.sql"
	exit 1
}

sql="$1"

[ -r "$sql" ] || {
	echo "Unable to read SQL file '$sql'"
	exit 1
}

d=`dirname $0`

db=$d/../medsearch.db

set -x
sqlite3 $db < "$sql"


