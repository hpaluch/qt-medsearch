#!/bin/bash

cat <<EOF
PRAGMA synchronous=OFF;
PRAGMA count_changes=OFF;
PRAGMA journal_mode=MEMORY;
PRAGMA temp_store=MEMORY;
BEGIN TRANSACTION;
EOF

iconv -f CP1250 -t UTF-8 | \
   awk -F \; '{ if(NR>1){
		   gsub("\"\"","\"",$3);
		   gsub("^\"","",$3);
		   gsub("\"$","",$3);
		   gsub("'"'"'","'"''"'",$3);
                   printf("insert into sukl_medicaments(sukl_code,name) values(%d,'"'%s'"');\n",
                   $1,$3)} }'
echo "COMMIT TRANSACTION;"



